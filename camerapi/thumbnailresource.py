from flask import send_file
from flask_restful import Resource
from pathlib import Path

from camerapi.thumbnailservice import ThumbnailService
import settings

class ThumbnailResource(Resource):
	def __init__(self):
		self.service = ThumbnailService(Path(settings.IMAGE_PATH), settings.THUMBNAIL_RESOLUTION)
		
	def get(self, image_name):
		thumb_file = self.service.get_thumbnail(image_name)
		
		response = send_file(str(thumb_file), attachment_filename=image_name, mimetype='image/jpeg')
		response.headers['content-type'] = 'image/jpeg'
		
		return response