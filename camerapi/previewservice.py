import io

import picamera
from flask_restful import Resource


class PreviewService:
	def get_preview(self):
		stream = io.BytesIO()
		with picamera.PiCamera() as camera:
			camera.resolution = (1024, 768)
			camera.capture(stream, 'jpeg')

		stream.seek(0)
		return stream
