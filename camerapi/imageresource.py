from flask import send_file
from flask_restful import Resource
from pathlib import Path

import settings

class ImageResource(Resource):
	def __init__(self):
		pass
		
	def get(self, image_name):
		image_path = Path(settings.IMAGE_PATH, image_name)
		response = send_file(str(image_path), attachment_filename=image_name, mimetype='image/jpeg')
		response.headers['content-type'] = 'image/jpeg'
		
		return response

