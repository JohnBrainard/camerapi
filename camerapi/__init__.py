from flask import Flask, request, send_file
from flask_restful import Api, Resource
from pathlib import Path

import io
import picamera

from camerapi.cameraresource import CameraResource
from camerapi.previewresource import PreviewResource
from camerapi.imagesresource import ImagesResource
from camerapi.imageresource import ImageResource
from camerapi.thumbnailresource import ThumbnailResource

app = Flask(__name__)
api = Api(app)

api.add_resource(PreviewResource, "/preview")
api.add_resource(CameraResource, "/camera")
api.add_resource(ImagesResource, "/images")
api.add_resource(ImageResource, "/images/<image_name>")
api.add_resource(ThumbnailResource, "/thumbnails/<image_name>")
