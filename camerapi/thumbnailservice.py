from pathlib import Path
from PIL import Image

class ThumbnailService:
	def __init__(self, image_path, thumb_resolution):
		self.image_path = image_path
		self.thumb_path = Path(image_path, '.thumbnails')
		self.thumb_resolution = thumb_resolution
		
		if not self.thumb_path.exists():
			self.thumb_path.mkdir()
	
	def get_thumbnail(self, image_name):
		thumb_file = Path(self.thumb_path, image_name)
		
		if not thumb_file.exists():
			image_file = Path(self.image_path, image_name)
			image = Image.open(str(image_file))
			thumb = image.copy()
			thumb.thumbnail(self.thumb_resolution)
			
			with thumb_file.open(mode='wb') as f:
				thumb.save(f, 'JPEG')

		return thumb_file