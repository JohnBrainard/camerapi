from flask import send_file
from flask_restful import Resource

from camerapi.previewservice import PreviewService

class PreviewResource(Resource):
	def __init__(self):
		self.service = PreviewService()
		
	def get(self):
		preview = self.service.get_preview()
		response = send_file(preview, attachment_filename='preview.jpg', mimetype='image/jpeg')
		response.headers['content-type'] = 'image/jpeg'
		
		return response
