from flask import Flask, request, send_file
from flask_restful import Api, Resource
from pathlib import Path

import io
import picamera

from cameraresource import CameraResource
from previewresource import PreviewResource
from imagesresource import ImagesResource
from imageresource import ImageResource
from thumbnailresource import ThumbnailResource

app = Flask(__name__)
api = Api(app)

api.add_resource(PreviewResource, "/preview")
api.add_resource(CameraResource, "/camera")
api.add_resource(ImagesResource, "/images")
api.add_resource(ImageResource, "/images/<image_name>")
api.add_resource(ThumbnailResource, "/thumbnails/<image_name>")

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=8000, debug=True)
