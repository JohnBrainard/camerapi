from flask import send_file, request
from flask_restful import Resource
from pathlib import Path

import settings

def _is_valid_image(image):
	return image.is_file() and not image.name.startswith('.')

class ImagesResource(Resource):
	def get(self):
		images_path = Path(settings.IMAGE_PATH)
		images = []
		
		for image in images_path.glob("*"):
			if _is_valid_image(image):
				images.append({
					"image":image.name,
					"uri":request.url_root + "images/" + image.name,
					"thumb_uri":request.url_root + "thumbnails/" + image.name
				})
		
		return { "images":images }

