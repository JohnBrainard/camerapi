from flask import request, send_file
from flask_restful import Resource

from camerapi.cameraservice import CameraService

import settings

class CameraResource(Resource):
	def __init__(self):
		self.service = CameraService(settings.RESOLUTION, settings.IMAGE_PATH)
	
	""" Capture an image and return its bytes """
	def get(self):
		capture = self.service.capture()
		response = send_file(capture, attachment_filename='capture.jpg', mimetype='image/jpeg')
		response.headers['content-type'] = 'image/jpeg'
		
		return response
		
	""" Capture an image and return its URI """
	def post(self):
		file = self.service.capture_to_file()
		
		return {
			"image_uri":request.url_root + "images/" + file
		}


	""" Capture an image at the requested URI """
	def put(self):
		pass

