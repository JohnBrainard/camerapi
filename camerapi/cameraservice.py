import io
import picamera

from datetime import datetime
from pathlib import Path

class CameraService:
	def __init__(self, resolution, image_path):
		self.resolution = resolution
		self.image_path = image_path
		
	def capture(self):
		stream = io.BytesIO()
		with picamera.PiCamera() as camera:
			camera.resolution = self.resolution
			camera.capture(stream, 'jpeg')

		stream.seek(0)
		return stream

	def capture_to_file(self, file_name=None):
		file_name = self._get_file_name(file_name)
		file = Path(self.image_path, file_name)
		
		with picamera.PiCamera() as camera:
			camera.resolution = self.resolution
			camera.capture(str(file))

		return file_name
		
	def _get_file_name(self, file_name):
		if file_name == None:
			now = datetime.now()
			name = now.strftime("%Y-%m-%d-%H-%M-%S-%f")
			
			file_name = name + ".jpg"
		
		return file_name
		
